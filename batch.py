import datetime
import json
import logging
import shutil
import traceback
import zipfile

import pymel.core as pm
from pymel.all import workspace
import maya.app.general.zipScene as zipScene


import click
from init_arnold import *
from maya_cmds import list_maya_file, scan_arnold_udim_textures, output_list_to_file
from zip7_cmds import archive as zip7

logging.basicConfig(filename='maya_batch_work.log', level=logging.WARNING,
                    format='%(asctime)s.%(msecs)d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")

VALID_EXT = [".ma", ".mb"]


@click.group()
def cli():
    pass


@cli.command()
@click.argument('text_file', nargs=1)
@click.option('--dst', nargs=1, default=".", help="Destination Folder")
@click.option('--jf', nargs=1, default=False, help="Load listing from a json file")
def read(text_file, dst, jf):
    if not os.path.isfile(text_file):
        logging.info("Source File \"{}\" does not exists".format(text_file))
        return

    if not os.path.isdir(dst):
        try:
            os.makedirs(dst)
        except IOError as e:
            logging.debug(e)
            return

    try:
        json_file = bool(jf)
    except ValueError as e:
        logging.debug(e)
        return

    if json_file:
        with open(text_file, "r") as reader:
            listing = [item.strip("\n") for item in json.load(reader)]
    else:
        with open(text_file, 'r') as reader:
            listing = [line.strip("\n").replace("/", os.sep) for line in reader.readlines()]

        for item in listing:
            if os.path.isfile(item):
                archive_maya_file(item, dst)


@cli.command(help="Accept a list of maya files and archive them into zip files, then copy them to the destination")
@click.argument('files', nargs=-1)
@click.option('--dst', nargs=1, default=".")
def archive(files, dst, func=None):
    # Inversion of Control, if no function passed we use default archive function
    if hasattr(func, "__call__"):
        archive_func = func
    else:
        archive_func = archive_maya_file

    if not os.path.isdir(dst):
        try:
            os.makedirs(dst)
        except IOError as e:
            logging.debug(e)
            return

    for file_ in files:
        try:
            archive_func(file_, dst)
        except Exception as e:
            logging.debug(e)
            continue


zip7_help = "Accept a list of maya files and archive them using 7Zip. "
zip7_help += "7Zip must install in the system, then copy them to the destination"


@cli.command(help=zip7_help)
@click.argument('files', nargs=-1)
@click.option('--dst', nargs=1, default=".")
def zip7(files, dst, func=zip7):
    # Inversion of Control, if no function passed we use default archive function
    if hasattr(func, "__call__"):
        archive_func = func
    else:
        archive_func = zip7

    if not os.path.isdir(dst):
        try:
            os.makedirs(dst)
        except IOError as e:
            logging.debug(e)
            return

    for maya_file in [item for item in files if os.path.isfile(item)]:

        print ("\nHandling {}\n".format(maya_file))

        __base = os.path.basename(maya_file)

        try:
            archive_func(maya_file, os.path.join(dst, __base))
        except Exception as e:
            logging.debug(e)
            continue


@cli.command(help="Accept a list of maya files and archive them into zip files, then copy them to the destination")
@click.argument('files', nargs=-1)
@click.option('--path', nargs=1, default="./maya_file_list.txt")
def list_file(files, path):
    for maya_file in files:
        files = list_maya_file(maya_file)
        output_list_to_file(maya_file, files=files, list_path=path)


"""
    Main Commands
"""


def archive_maya_file(file_, dst):
    print "\n\n{} Archiving Maya files, then copy to Destination {}\n".format("=" * 5, "=" * 5)
    print "{} Maya File: {} {}".format("=" * 5, file_, "=" * 5)
    print "{} Destination: {} {}\n\n".format("=" * 5, dst, "=" * 5)

    if os.path.isfile(file_):
        if os.path.splitext(file_)[-1] in VALID_EXT:
            file_dir = os.path.dirname(file_)
            file_base, file_ext = os.path.splitext(os.path.basename(file_))

            # 0.0.1: Force open file since we are in batch mode
            try:
                pm.openFile(file_, force=True)
            except Exception as e:
                logging.debug(e)
                return

            # 0.0.1: Change workspace to the archiving folder, avoid archiving other folders and workspace.mel file
            try:
                workspace.chdir(file_dir)
                workspace.save()
            except Exception as e:
                logging.debug(e)

            temp_fn = "{}.{}{}".format(file_base, datetime.date.today(), file_ext)
            temp_fn = os.path.join(file_dir, temp_fn)
            temp_fn_zip_name = "{}.zip".format(temp_fn)
            dst_zip_name = "{}{}.zip".format(os.path.join(dst, file_base), file_ext)

            # get the list of files that is listed using udim token for arnold in Maya 2015

            udim_file_list = scan_arnold_udim_textures()

            try:
                pm.saveAs(temp_fn)
                zipScene.zipScene(1)

                if os.path.isfile(temp_fn_zip_name):

                    try:

                        shutil.move(temp_fn_zip_name, dst_zip_name)

                        # append udim file to the zip archive created by Maya
                        if udim_file_list:
                            print "\nProcessing UDIM Textures ... \n"
                            for item in udim_file_list:
                                logging.info("Processing UDIM: {}".format(item))
                                try:
                                    zip_file = zipfile.ZipFile(dst_zip_name, "a",
                                                               zipfile.ZIP_DEFLATED,
                                                               allowZip64=True, )

                                    zip_file.write(item)
                                    zip_file.close()

                                except IOError as e:
                                    logging.debug(e)
                                    print "Fail to add {} to destination zip file"

                    except IOError as e:
                        logging.debug(e)
                        print "Cannot copy to {}".format(dst)
                    finally:
                        os.remove(temp_fn)
                        logging.info("\nMoved temp Maya file: \"{}\" into {}\n".format(temp_fn_zip_name, dst))
                else:
                    logging.info("Zip archive does not exist or destination does not exists!!")

            except Exception as e:
                logging.debug(e)
                logging.exception("Exception Msg: \n")
                traceback.print_exc(file=sys.stdout)
                traceback.print_stack(file=sys.stdout)

            finally:
                pm.newFile()

        else:
            logging.info("\"{}\" is not a Maya file".format(file_))
    else:
        print logging.info("Maya File: \"{}\", doesn't exists".format(file_))


if __name__ == "__main__":
    cli()
