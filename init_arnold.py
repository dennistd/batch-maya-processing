import os
import sys

local_arnold_script_path = "C:/solidangle/mtoadeploy/2015/scripts"

if os.path.isdir(local_arnold_script_path):
    sys.path.append(local_arnold_script_path)

else:
    sys.exit()
