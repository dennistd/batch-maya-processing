import codecs
import logging
import os

import maya.cmds as cmds
import pymel.core as pm

logging.basicConfig(filename='maya_cmds.log', level=logging.WARNING,
                    format='%(asctime)s.%(msecs)d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")


def scan_arnold_udim_textures(key="<udim>"):
    """
    Scan file texture node for Arnold UDIM token
    :return: list of file texture path
    """

    final_file_list = []

    for file_tex in pm.ls(type="file"):
        tex_path = file_tex.fileTextureName.get()
        tex_dir = os.path.dirname(tex_path)
        tex_base = os.path.basename(tex_path)

        if key in tex_path and os.path.isdir(tex_dir):

            udim_base, udim_ext = tex_base.split(key)

            file_list = [os.path.join(tex_dir, item) for item in os.listdir(tex_dir)]

            for item in file_list:
                if udim_base in item:
                    final_file_list.append(item)

    final_file_list = sorted(list(set(final_file_list)))

    return final_file_list


def sum_files_size(file_list):
    # get the size of each file
    return sum([os.stat(_file).st_size for _file in file_list])


def list_maya_file(maya_file):
    if os.path.isfile(maya_file):
        # 0.0.1: Force open file since we are in batch mode
        try:
            pm.openFile(maya_file, force=True)
        except Exception as e:
            logging.debug(e)
            return
    # scan arnold udim texture
    udim_file_list = scan_arnold_udim_textures()

    # get a list of all the files associated with the scene
    files = cmds.file(query=1, list=1, withoutCopyNumber=1)
    files = files + udim_file_list

    files = sorted(list(set(files)))

    return files


def output_list_to_file(maya_file, files, list_path):
    # by Dennis
    # write out a list of files to be archived

    total_size = sum_files_size(files)

    if not os.path.isdir(os.path.dirname(list_path)):
        os.makedirs(os.path.dirname(list_path))

    with codecs.open(list_path, "a", encoding='utf8') as writer:
        writer.write(maya_file + "\n\n")
        writer.write("TOTAL SIZE: {} MBs".format(int(total_size) / 1000000))
        writer.write("\n" + "=" * 80 + "\n")
        for item in files:
            writer.write(item + "\n")
        writer.write("\n" + "=" * 80 + "\n")

    return


if __name__ == "__main__":
    pass
